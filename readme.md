# Grow# #

##Grow Sharp Base controller Project.

This repository has the EFM32WG files for the base node. This node is based on a EFM32WG board from SiliconLabs and has all the files included in the SimplicityStudio project.
This node handles the communication between the RaspberryPi and the base node. It also communicates via ez radio to the HydroponicNode [see here](https://bitbucket.org/jmbranco76/growsharp_hydroponic_node).

Board:  Silicon Labs SLWSTK6220A_EZR32WG Development Kit
Device: EZR32WG330F256R60
