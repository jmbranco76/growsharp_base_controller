/*
 * send_raspi.h
 *
 *  Created on: 7 de Nov de 2015
 *      Author: Jo�o Sousa
 */

#ifndef SEND_RASPI_H_
#define SEND_RASPI_H_

#include "sensor_ids.h"
#include "stuff.h"

void initSensorsAddrs();
void sendSensorID(int id, int lot, int value);
uint8_t sendSensors();

#endif /* SEND_RASPI_H_ */
