/*
 * sensor_ids.h
 *
 *  Created on: 7 de Nov de 2015
 *      Author: Jo�o Sousa
 */

#ifndef SENSOR_IDS_H_
#define SENSOR_IDS_H_

#define NUMBER_OF_SENSORS	18

#define MAIN_RESERVOIR_LOW_LEVEL		1
#define MAIN_RESERVOIR_HIGH_LEVEL		2
#define MAIN_RESERVOIR_PUMP				3
#define TEMPERATURE_SI7021				4
#define HUMIDITY_SI7021					5
#define NTC_1							6 // cima
#define NTC_2							7
#define NTC_3							8 // baixo
#define NTC_4							9
#define LED_HYPER_RED					10	//
#define LED_FAR_RED						11
#define LED_DEEP_BLUE					12
#define PH								13
#define EC								14
#define PV_VOLTAGE						15
#define PV_CURRENT						16
#define BAT_VOLTAGE						17
#define BAT_CURRENT						18

#define MAIN_RESERVOIR_LOW_LEVEL_LEN	1
#define MAIN_RESERVOIR_HIGH_LEVEL_LEN	1
#define MAIN_RESERVOIR_PUMP_LEN			1
#define TEMPERATURE_SI7021_LEN			2
#define HUMIDITY_SI7021_LEN				4
#define NTC_1_LEN						2
#define NTC_2_LEN						2
#define NTC_3_LEN						2
#define NTC_4_LEN						2
#define LED_HYPER_RED_LEN				1
#define LED_FAR_RED_LEN					1
#define LED_DEEP_BLUE_LEN				1
#define PH_LEN							2
#define EC_LEN							2
#define PV_VOLTAGE_LEN					4
#define PV_CURRENT_LEN					4
#define BAT_VOLTAGE_LEN					4
#define BAT_CURRENT_LEN					4

#define READING_MESSAGE					'1'


//#define MAIN_RESERVOIR_LOW_LEVEL	(0)
//#define MAIN_RESERVOIR_HIGH_LEVEL	(1)
//#define MAIN_RESERVOIR_PUMP			(2)
//#define TEMPERATURE_SI7021			(3)
//#define HUMIDITY_SI7021				(4)
//#define NTC_1						(5)
//#define NTC_2						(6)
//#define NTC_3						(7)
//#define NTC_4						(8)
//#define LED_HYPER_RED				(9)
//#define LED_FAR_RED					(10)
//#define LED_DEEP_BLUE				(11)
//#define PH							(12)
//#define EC							(13)
//#define
//#define
//#define
//#define
//#define
//#define
//#define
//#define
//#define
//#define


#endif /* SENSOR_IDS_H_ */
