/*
 * nutrient.h
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#ifndef NUTRIENT_H_
#define NUTRIENT_H_

#include "stuff.h"
#include "datastructures.h"


uint8_t putNutrient(int id, int ml);
Nutrient_State nutrientSpinFunction();
uint8_t setControlMode(ControlMode_type mode);
uint8_t spinReadSensors();
void updateMeasurementF();
void newWirelessData();
HydroponicNode_type *getHydroNodePtr();

Pump_State getMainPumpState();
Pump_State getAirBubbleState();
Pump_State getPeristalticPumpState();
Pump_State setMainPumpState(Pump_State new_state);
Pump_State setAirBubbleState(Pump_State new_state);
Pump_State setPeristalticState(Pump_State new_state);
uint8_t setMainPumpTimes(uint32_t on, uint32_t off);
uint8_t setAirBubblesPumpTimes(uint32_t on, uint32_t off);
uint8_t putPeristalticPump(uint32_t time_on_ms);
void pumpSpinFunction();

#endif /* NUTRIENT_H_ */
