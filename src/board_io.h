/*
 * board_io.h
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#ifndef BOARD_IO_H_
#define BOARD_IO_H_

#include "stuff.h"

#ifdef __cplusplus
extern "C" {
#endif

#define DEPLOY_VALVE_PORT		gpioPortC
#define DEPLOY_VALVE_PIN		6
#define NUTRIENT1_VALVE_PORT	gpioPortE
#define NUTRIENT1_VALVE_PIN		2
#define NUTRIENT2_VALVE_PORT	gpioPortF
#define NUTRIENT2_VALVE_PIN		4
#define NUTRIENT3_VALVE_PORT	gpioPortF
#define NUTRIENT3_VALVE_PIN		3

#define WATER_LOW_LEVEL_PORT	gpioPortB
#define WATER_LOW_LEVEL_PIN		11
#define WATER_HIGH_LEVEL_PORT	gpioPortA
#define WATER_HIGH_LEVEL_PIN	13

#define MAIN_PUMP_PORT			gpioPortE
#define MAIN_PUMP_PIN			3
#define AIR_PUMP_PORT			gpioPortC
#define AIR_PUMP_PIN			7
#define PERISTALTIC_PUMP_PORT	gpioPortF
#define PERISTALTIC_PUMP_PIN	7

#define I2C_periph				I2C1


#define NUTRIENT1				1
#define NUTRIENT2				2
#define NUTRIENT3				3
#define WATER_NUTRIENT			0


void initIO(void);
void deployValve(uint8_t state);
void nutrientValve(uint8_t state, uint8_t id);
uint8_t getHighLevelSensor();
uint8_t getLowLevelSensor();
const uint8_t *getHighLevelSensorPtr();
const uint8_t *getLowLevelSensorPtr();

#ifdef __cplusplus
}
#endif

#endif /* BOARD_IO_H_ */
