/*
 * hall_reading.h
 *
 *  Created on: 25 de Ago de 2015
 *      Author: Jos�
 */

#ifndef HALL_READING_H_
#define HALL_READING_H_

#include "stuff.h"

#ifdef __cplusplus
extern "C" {
#endif

/** DMA channel used for scan sequence sampling adc channel 2, 3 and 4. */
#define DMA_CHANNEL    4
#define NUM_SAMPLES    8

//#define ENABLE_DEBUG

typedef enum
{
	Measuring = 0, MeasureOk, ErrorMeasuring
}MeasureState;

typedef enum {
	Init_State = 0,
	Default,
	Check_For_False_Values,
	Wait_For_More_Values,
	Check_For_Continuous,
	Update_Last_Sensor,
	Error_State,
	Error_Check_For_False_Values,
	Error_Wait_For_More_Values
}HallSensor_States;


void setCalib(I2C_TypeDef *i2c);
void getCalib(I2C_TypeDef *i2c);
uint32_t * getSensors();
HallSensor_States updateSensor(int limit, int8_t *cont_result);
MeasureState updateNlinmitSensors(uint8_t * vector, uint8_t * vector_sum,
		uint8_t n_valid_measures, uint8_t reset_state);
int8_t getSensorUpperLimit(int limit, uint8_t *results);
int8_t checkForContinuity(uint8_t last_sensor, uint8_t *n_times_sensor_sum);
int getLevel();
extern uint32_t readSensors(void);

#ifdef __cplusplus
}
#endif

#endif /* HALL_READING_H_ */
