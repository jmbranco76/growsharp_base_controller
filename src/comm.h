/*
 * comm.h
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#ifndef COMM_H_
#define COMM_H_

#include "stuff.h"
#include "datastructures.h"

#include "json.h"

#define BUFFER_SIZE				50
#define CONTROL_MESSAGE_DELTA	1000

void parseStruct(json_value *value);
void parseCmd();
Control_Node *getControlMessagePtr();
Photovoltaic_type *getPhotovoltaicMessagePtr();
Control_Node *getControllNodeEchoPtr();
uint8_t checkControlMessageEcho();
void newPVData();

#endif /* COMM_H_ */
