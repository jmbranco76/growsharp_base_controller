/*
 * time_board.h
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#ifndef TIME_BOARD_H_
#define TIME_BOARD_H_

#include "stuff.h"

void addTime();
uint32_t getTime();
uint32_t setUnixTimestamp(uint32_t t);
uint32_t getUnixTimestamp();

#endif /* TIME_BOARD_H_ */
