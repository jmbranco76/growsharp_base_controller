/*
 * send_raspi.cpp
 *
 *  Created on: 7 de Nov de 2015
 *      Author: Jos�
 */

#include "send_raspi.h"
#include "retargetserial.h"

uint32_t sensors_to_send = 0x3FFF;
uint32_t *sensor_value_addr[NUMBER_OF_SENSORS];
uint8_t sensor_value_addr_len[NUMBER_OF_SENSORS];
char array[100];
char array_base64[133];

void initSensorsAddrs()
{
	sensor_value_addr[MAIN_RESERVOIR_LOW_LEVEL - 1] = (uint32_t*)getLowLevelSensorPtr();
	sensor_value_addr[MAIN_RESERVOIR_HIGH_LEVEL - 1] = (uint32_t*)getHighLevelSensorPtr();
	sensor_value_addr[MAIN_RESERVOIR_PUMP - 1] = NULL;
	sensor_value_addr[TEMPERATURE_SI7021 - 1] = (uint32_t *)&(getHydroNodePtr()->temp[4]);
	sensor_value_addr[HUMIDITY_SI7021 - 1] = (uint32_t *)getHydroNodePtr()->rh;
	sensor_value_addr[NTC_1 - 1] = (uint32_t *)&(getHydroNodePtr()->temp[0]);
	sensor_value_addr[NTC_2 - 1] = (uint32_t *)&(getHydroNodePtr()->temp[1]);
	sensor_value_addr[NTC_3 - 1] = (uint32_t *)&(getHydroNodePtr()->temp[2]);
	sensor_value_addr[NTC_4 - 1] = (uint32_t *)&(getHydroNodePtr()->temp[3]);
	sensor_value_addr[LED_HYPER_RED - 1] = NULL;
	sensor_value_addr[LED_FAR_RED - 1] = NULL;
	sensor_value_addr[LED_DEEP_BLUE - 1] = NULL;
	sensor_value_addr[PH - 1] = (uint32_t *)&(getHydroNodePtr()->ph);
	sensor_value_addr[EC - 1] = (uint32_t *)&(getHydroNodePtr()->ec);
	sensor_value_addr[PV_VOLTAGE - 1] = (uint32_t *)&(getPhotovoltaicMessagePtr()->pv_v);
	sensor_value_addr[PV_CURRENT - 1] = (uint32_t *)&(getPhotovoltaicMessagePtr()->pv_i);
	sensor_value_addr[BAT_VOLTAGE - 1] = (uint32_t *)&(getPhotovoltaicMessagePtr()->bat_v);
	sensor_value_addr[BAT_CURRENT - 1] = (uint32_t *)&(getPhotovoltaicMessagePtr()->bat_i);

	sensor_value_addr_len[MAIN_RESERVOIR_LOW_LEVEL - 1] = MAIN_RESERVOIR_LOW_LEVEL_LEN;
	sensor_value_addr_len[MAIN_RESERVOIR_HIGH_LEVEL - 1] = MAIN_RESERVOIR_HIGH_LEVEL_LEN;
	sensor_value_addr_len[MAIN_RESERVOIR_PUMP - 1] = MAIN_RESERVOIR_PUMP_LEN;
	sensor_value_addr_len[TEMPERATURE_SI7021 - 1] = TEMPERATURE_SI7021_LEN;
	sensor_value_addr_len[HUMIDITY_SI7021 - 1] = HUMIDITY_SI7021_LEN;
	sensor_value_addr_len[NTC_1 - 1] = NTC_1_LEN;
	sensor_value_addr_len[NTC_2 - 1] = NTC_2_LEN;
	sensor_value_addr_len[NTC_3 - 1] = NTC_3_LEN;
	sensor_value_addr_len[NTC_4 - 1] = NTC_4_LEN;
	sensor_value_addr_len[LED_HYPER_RED - 1] = LED_HYPER_RED_LEN;
	sensor_value_addr_len[LED_FAR_RED - 1] = LED_FAR_RED_LEN;
	sensor_value_addr_len[LED_DEEP_BLUE - 1] = LED_DEEP_BLUE_LEN;
	sensor_value_addr_len[PH - 1] = PH_LEN;
	sensor_value_addr_len[EC - 1] = EC_LEN;
	sensor_value_addr_len[PV_VOLTAGE - 1] = PV_VOLTAGE_LEN;
	sensor_value_addr_len[PV_CURRENT - 1] = PV_CURRENT_LEN;
	sensor_value_addr_len[BAT_VOLTAGE - 1] = BAT_VOLTAGE_LEN;
	sensor_value_addr_len[BAT_CURRENT - 1] = BAT_CURRENT_LEN;
}

void sendSensorID(int id, int lot, int value)
{
	int n = sprintf(array, "{\"id\":%d,\"sensor\":%d,\"value\":%d}", id, lot, value);
	n = encryptData((uint8_t *)array, n);
	int out_size = 133;
	base64_encode(array_base64, out_size, (const uint8_t *)array, n);
	for (unsigned int i = 0; i < sizeof(array_base64); ++i)
	{
		RETARGET_WriteChar((char)array_base64[i]);
	}
	RETARGET_WriteChar('\n');
}

uint8_t sendSensors()
{
	uint8_t counter = 0;
	for (int i = 0; i < NUMBER_OF_SENSORS; ++i)
	{
		if(((sensors_to_send & (0x0001<<i)) != 0) && (sensor_value_addr[i] != NULL))
		{
			int value =0;
			memcpy(&value, sensor_value_addr[i], sensor_value_addr_len[i]);
			sendSensorID(READING_MESSAGE, i+1, value);
			counter++;
		}
	}
	return counter;
}
