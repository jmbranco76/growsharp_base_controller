/*
 * nutrient.cpp
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#include "nutrient.h"

Nutrient_State nutrient_state = Idle_nutrient_state;
uint8_t nutrient_ml = 0;
int8_t nutrient_id = 0;
ControlMode_type control_mode = ControlAuto;
static bool updateMeasurement = true;
static bool new_wireless_data = false;
int counter =0, counter2 =0;
HydroponicNode_type node_h;

// main pump state
Pump_State main_pump_state = PumpOn;
Pump_State air_bubble_state = PumpOn;
Pump_State peristaltic_state = PumpOff;
uint32_t main_pump_time = getTime();
uint32_t air_pump_time = getTime();
uint32_t peristaltic_pump_time = getTime();
//uint32_t main_pump_time_on = 20000, main_pump_time_off = 20000;
//uint32_t air_bubble_pump_time_on = 0, air_bubble_pump_time_off = 1000;
uint32_t peristaltic_pump_time_on = 0;
uint32_t warning_last_time = 0, warning_delta = 5000;



uint32_t main_pump_time_on = 900000, main_pump_time_off = 900000;
uint32_t air_bubble_pump_time_on = 43200000, air_bubble_pump_time_off = 43200000;





uint8_t putNutrient(int id, int ml)
{
	if(nutrient_state != 0 && id > 0 && id <= 3)
	{
		nutrient_id = id;
		nutrient_ml = ml;
		return -1;
	}
	nutrient_id = id;
	nutrient_ml = ml;
	nutrient_state = Fill_water_before;
	control_mode = ControlAuto;
	return 0;
}

Nutrient_State nutrientSpinFunction()
{
	static uint32_t last_time = getTime();

	if(control_mode == ControlAuto)		// mode auto
	{
		switch (nutrient_state) {
		case Idle_nutrient_state:
#ifdef ENABLE_DEBUG
			printf("Nutrient: idle\n");
#endif
			break;
		case Fill_water_before:
#ifdef ENABLE_DEBUG
			printf("Nutrient: Fill water\n");
#endif
			// open water valve
			nutrientValve(1, WATER_NUTRIENT);
			if(getLevel() > 4)	// fill water upto 2 ml
			{
				// close valve
				nutrientValve(0, 3);
				nutrient_state = Fill_nutrient;
			}
			break;
		case Fill_nutrient:
#ifdef ENABLE_DEBUG
			printf("Nutrient: fill nutrient\n");
#endif
			// open nutrient valve
			nutrientValve(1, nutrient_id);
			if(getLevel() > (4+nutrient_ml))
			{
				// close valve
				nutrientValve(0, nutrient_id);
				nutrient_state = Deploy_nutrient;
				last_time = getTime();
			}
			break;
		case Deploy_nutrient:
#ifdef ENABLE_DEBUG
			printf("Nutrient: deploy nutrient\n");
#endif
			// open deploy valve
			deployValve(1);
			if((getLevel() <= 0 && ((getTime() - last_time) > 5000)) || ((getTime() - last_time) > 10000))
				//			if((getLevel() <= 0) || (getTime() - last_time > 3000))
			{
				// close deploy valve
				deployValve(0);
				nutrient_state = Fill_water_after;
			}
			break;
		case Fill_water_after:
#ifdef ENABLE_DEBUG
			printf("Nutrient: fill water after\n");
#endif
			//open water valve
			nutrientValve(1, WATER_NUTRIENT);
			if(getLevel() > 10)
			{
				// close water valve
				nutrientValve(0, 3);
				nutrient_state = Deploy_water;
				last_time = getTime();
			}
			break;
		case Deploy_water:
#ifdef ENABLE_DEBUG
			printf("Nutrient: deploy water\n");
#endif
			// open deploy valve
			deployValve(1);
			if((getLevel() <= 0 && ((getTime() - last_time) > 5000)) || ((getTime() - last_time) > 10000))
			{
				// close deploy valve
				deployValve(0);
				nutrient_state = Idle_nutrient_state;
				nutrient_id = 0;
				nutrient_ml = 0;
			}

			break;
		default:
			nutrient_state = Idle_nutrient_state;
			break;
		}
	}
	return nutrient_state;
}

uint8_t setControlMode(ControlMode_type mode)
{
	control_mode = mode;
}

uint8_t spinReadSensors()
{
	if(counter2++ > 10000)
	{
		nutrientSpinFunction();
		counter2 = 0;
	}
	if(updateMeasurement)
	{
		updateMeasurement = false;
		readSensors();

		int8_t sensor_index;

		HallSensor_States state = updateSensor(150, &sensor_index);
		if(counter++ > 100)
		{
			counter = 0;
//			printf("Sensor: %d\n", getTime());
//			printf("Sensor: %d, state %d\n", sensor_index, state);
		}

	}
}

void updateMeasurementF()
{
	updateMeasurement = true;
}

void newWirelessData()
{
	new_wireless_data = true;
	HydroponicNode_type *hidro = &node_h;
	printf("temp %d %d %d %d | %d %d\n", hidro->temp[0],hidro->temp[1],hidro->temp[2],hidro->temp[3],hidro->temp[4],hidro->rh);
//	printf("DAta Wireless ph%d, temp%d, rh%d\n", node_h.ph, node_h.temp[0], node_h.rh);
}

HydroponicNode_type *getHydroNodePtr()
{
	return &node_h;
}

Pump_State getMainPumpState()
{
	return main_pump_state;
}

Pump_State getAirBubbleState()
{
	return air_bubble_state;
}

Pump_State getPeristalticPumpState()
{
	return peristaltic_state;
}

Pump_State setMainPumpState(Pump_State new_state)
{
	main_pump_state = new_state;
	main_pump_time = getTime();
	if(main_pump_state == PumpOn)
	{
		GPIO_PinOutClear(MAIN_PUMP_PORT, MAIN_PUMP_PIN);
	}
	else
	{
		GPIO_PinOutSet(MAIN_PUMP_PORT, MAIN_PUMP_PIN);
	}
	return main_pump_state;
}

Pump_State setAirBubbleState(Pump_State new_state)
{
	air_bubble_state = new_state;
	air_pump_time = getTime();
	if(air_bubble_state == PumpOn)
	{
		GPIO_PinOutClear(AIR_PUMP_PORT, AIR_PUMP_PIN);
	}
	else
	{
		GPIO_PinOutSet(AIR_PUMP_PORT, AIR_PUMP_PIN);
	}
	return air_bubble_state;
}

Pump_State setPeristalticState(Pump_State new_state)
{
	peristaltic_state = new_state;
	if(peristaltic_state == PumpOn)
	{
		GPIO_PinOutClear(PERISTALTIC_PUMP_PORT, PERISTALTIC_PUMP_PIN);
	}
	else
	{
		GPIO_PinOutSet(PERISTALTIC_PUMP_PORT, PERISTALTIC_PUMP_PIN);
	}
	return peristaltic_state;
}

uint8_t setMainPumpTimes(uint32_t on, uint32_t off)
{
	main_pump_time_on = on;
	main_pump_time_off = off;

	if((main_pump_time_on == 0) && (main_pump_time_off == 0))
	{
		setMainPumpState(PumpOff);
		return 1;
	}
	return 0;
}

uint8_t setAirBubblesPumpTimes(uint32_t on, uint32_t off)
{
	air_bubble_pump_time_on = on;
	air_bubble_pump_time_off = off;

	if((air_bubble_pump_time_on == 0) && (air_bubble_pump_time_off == 0))
	{
		setAirBubbleState(PumpOff);
		return 1;
	}
	return 0;
}

uint8_t putPeristalticPump(uint32_t time_on_ms)
{
	peristaltic_pump_time_on = time_on_ms;

	if(peristaltic_pump_time_on == 0)
	{
		setPeristalticState(PumpOff);
		return 1;
	}
	else
	{
		setPeristalticState(PumpOn);
	}
	return 0;
}

void pumpSpinFunction()
{
	uint32_t now = getTime();

	if(now - warning_last_time > warning_delta)
	{
		warning_last_time = now;
		if(getLowLevelSensor() == 1)
		{
#ifdef ENABLE_DEBUG
			printf("Low water level\n");
#endif
		}
		if(getHighLevelSensor() == 0)
		{
#ifdef ENABLE_DEBUG
			printf("High water level\n");
#endif
		}
	}

	// if water between acceptable levels
	if(getLowLevelSensor() == 0) // above low level
	{
		// main pump
		if((main_pump_time_on != 0) && (main_pump_time_off != 0))
		{
			if(main_pump_state == PumpOn)
			{
				if(now - main_pump_time > main_pump_time_on)
				{
					main_pump_state = PumpOff;
					setMainPumpState(main_pump_state);
					main_pump_time = getTime();
				}
			}
			else
			{
				if(now - main_pump_time > main_pump_time_off)
				{
					main_pump_state = PumpOn;
					setMainPumpState(main_pump_state);
					main_pump_time = getTime();
				}
			}
		}

		// air bubble pump
		if((air_bubble_pump_time_on != 0) && (air_bubble_pump_time_off != 0))
		{
			if(air_bubble_state == PumpOn)
			{
				if(now - air_pump_time > air_bubble_pump_time_on)
				{
					air_bubble_state = PumpOff;
					setAirBubbleState(air_bubble_state);
					air_pump_time = getTime();
				}
			}
			else
			{
				if(now - air_pump_time > air_bubble_pump_time_off)
				{
					air_bubble_state = PumpOn;
					setAirBubbleState(air_bubble_state);
					air_pump_time = getTime();
				}
			}
		}
	}
	else
	{
		main_pump_state = PumpOff;
		setMainPumpState(main_pump_state);
		air_bubble_state = PumpOff;
		setAirBubbleState(air_bubble_state);
	}

	// peristaltic pump
	if(peristaltic_pump_time_on > 0)
	{
		if(peristaltic_state == PumpOn)
		{
			if(now - peristaltic_pump_time > peristaltic_pump_time_on)
			{
				setPeristalticState(PumpOff);
				peristaltic_pump_time_on = 0;
			}
		}
	}
	else
	{
		setPeristalticState(PumpOff);
		peristaltic_pump_time = now;
	}
}

