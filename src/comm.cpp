/*
 * comm.cpp
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#include "comm.h"

uint8_t parse_buffer[BUFFER_SIZE + 2];
uint8_t i_ptr = 0;
Control_Node ctrl_msg, ctrl_msg_echo;
Photovoltaic_type pv_message;
uint32_t sensors_delta = 10000;

uint32_t last_message = 0;
uint32_t last_sensors = 0;
uint32_t last_send_control = 0;

bool new_pv_data = false;

typedef enum
{
	NutrientID = 0,
	Quantity,
	DeployValve,
	NutrientValve,
	MainPump
}RaspiKeys_type;

typedef enum
{
	SetControlMode = 0,
	PutNutrient,
	ControlValve,
	ControlPump
}RaspiAction_type;

void parseCmd() {

	int value = 0, value2 = 0, value3;
	int c;

	while ((c = RETARGET_ReadChar()) != -1 && i_ptr < BUFFER_SIZE) {
		parse_buffer[i_ptr++] = (uint8_t) c;
		parse_buffer[i_ptr] = 0;
		char * endstr = NULL;
		char * begstr = NULL;
		endstr = strstr((char *) parse_buffer, "\r\n");
		if (endstr != NULL) {
			last_message = getTime();
			i_ptr = 0;
			char *pch = strstr((char *) parse_buffer, "=");
			char *pch2;
			if (pch != NULL) {
				if (strstr((char *) parse_buffer, "NU") !=NULL) {
					value = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");
					value2 = atoi(pch + 1);
					if (putNutrient(value, value2) &&
							strstr((char *)parse_buffer,",")==NULL) {
						printf("#ERROR: Nutrient value\n");
					} else {
						printf("#INFO: nutrient id = %d, %d ml\n", value,
								value2);
					}
					//TODO:
				} else if (strstr((char *) parse_buffer, "CT") !=NULL) {
					value = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");
					value2 = atoi(pch + 1);
					if (value >= 1 && value <= 4) {
						printf("#ERROR: Control mode\n");
					} else {
						printf("#INFO: LED control mode = %d, pwm %d, 0=daylight, 1=overide\n", value2, value);
						ctrl_msg.force_pwm[value] = value2?1:0;
					}
				} else if (strstr((char *) parse_buffer, "VA") !=NULL) {
					value = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");
					value2 = atoi(pch + 1);
					nutrientValve(value,value2);
					printf("#INFO: Valve = %d, state %d\n", value2, value);
				}
				else if (strstr((char *) parse_buffer, "DV") !=NULL) {
					value = atoi(pch + 1);
					deployValve(value);
					printf("#INFO:deploy: state %d\n", value);
				}
				else if (strstr((char *) parse_buffer, "TS") !=NULL) {
					value = atoi(pch + 1);
					setUnixTimestamp(value);
					printf("unix timestamp: %d\n", value);
				}
				else if (strstr((char *) parse_buffer, "PE") !=NULL) {
					value = atoi(pch + 1);
					putPeristalticPump(value);
					printf("#perist: ms %d\n", value);
				}
				else if (strstr((char *) parse_buffer, "ST") !=NULL) {
					value = atoi(pch + 1);
					if(value == 0 || value >= 1000)
					{
						sensors_delta = value;
						last_sensors = getTime();
						printf("#Stream: ms %d\n", value);
					}
				}
				else if (strstr((char *) parse_buffer, "LE") !=NULL) {
					value	 = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");
					value2 = atoi(pch + 1);
					pch2 = strstr((char *) pch + 1, ",");
					value3 = atoi(pch2 + 1);
					printf("#PWMs: %d %d %d\n", value, value2, value3);

					ctrl_msg.pwm1[0] = value;
					ctrl_msg.pwm2[0] = value2;
					ctrl_msg.pwm3[0] = value3;
					//TODO:
				}
				else if (strstr((char *) parse_buffer, "WL") !=NULL) {
					value	 = atoi(pch + 1);
					printf("#white led: %d\n", value);

					ctrl_msg.pwm4[0] = value;
				}
				else if (strstr((char *) parse_buffer, "PU") !=NULL) {
					value = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");
					value2 = atoi(pch + 1);
					printf("#main pump: %d %d\n", value, value2);

					setMainPumpTimes(value*1000, value2*1000);
				}
				else if (strstr((char *) parse_buffer, "BA") !=NULL) {
					value = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");
					value2 = atoi(pch + 1);
					printf("#air: %d %d %d\n", value, value2, value3);

					setAirBubblesPumpTimes(value*1000, value2*1000);
				}
				else if (strstr((char *) parse_buffer, "SB") !=NULL) {
					value = atoi(pch + 1);
					if(value == 0 || value == 1)
					{
						printf("#estado bomba agua: %d\n", value);
						setMainPumpState((Pump_State)!value);
					}
				}
				else if (strstr((char *) parse_buffer, "SA") !=NULL) {
					value = atoi(pch + 1);
					if(value == 0 || value == 1)
					{
						printf("#estado bomba ar: %d\n", value);
						setAirBubbleState((Pump_State)!value);
					}
				}
			}
		}
	}

	if(getTime() - last_sensors > sensors_delta && sensors_delta != 0)
	{
		last_sensors = getTime();
		sendSensors();//HydroponicNode_type *hidro = getHydroNodePtr();
		//		printf("temp %d %d %d %d | %d %d\n", hidro->temp[0],hidro->temp[1],hidro->temp[2],hidro->temp[3],hidro->temp[4],hidro->rh);
	}

	if(getTime() - last_send_control > CONTROL_MESSAGE_DELTA)
	{
		last_send_control = getTime();

		sendControlMessage(getControlMessagePtr());
	}

}

Photovoltaic_type *getPhotovoltaicMessagePtr()
{
	return &pv_message;
}

Control_Node *getControlMessagePtr()
{
	return &ctrl_msg;
}

Control_Node *getControllNodeEchoPtr()
{
	return &ctrl_msg_echo;
}

uint8_t checkControlMessageEcho()
{

}

void newPVData()
{
	new_pv_data = true;
}
