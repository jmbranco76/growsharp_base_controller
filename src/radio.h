/*
 * radio.h
 *
 *  Created on: 24 de Out de 2015
 *      Author: Jos�
 */

#ifndef RADIO_H_
#define RADIO_H_

#include "datastructures.h"
#include "stuff.h"

uint8_t sendControlMessage(Control_Node *ctrl_cfmsg);

void init_radios();
void RTC_App_IRQHandler();
int RepeatCallbackRegister (void(*pFunction)(void*),
                            void* pParameter,
                            unsigned int frequency);


#endif /* RADIO_H_ */
