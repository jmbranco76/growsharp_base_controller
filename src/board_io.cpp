/*
 * board_io.c
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#include "board_io.h"

uint8_t low_level_sensor = 0;
uint8_t high_level_sensor = 0;

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB11)
 *        Switches between analog and digital clock modes.
 *****************************************************************************/
void WATER_LOW_LEVEL_PIN_irq( uint8_t pin )
{
	(void)pin;
	low_level_sensor = GPIO_PinInGet(WATER_LOW_LEVEL_PORT, WATER_LOW_LEVEL_PIN);
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PC7)
 *        Switches between analog and digital clock modes.
 *****************************************************************************/
void WATER_HIGH_LEVEL_PIN_irq( uint8_t pin )
{
	(void)pin;
	high_level_sensor = GPIO_PinInGet(WATER_HIGH_LEVEL_PORT, WATER_HIGH_LEVEL_PIN);
}

void initIO(void)
{
	GPIO_PinModeSet(DEPLOY_VALVE_PORT, DEPLOY_VALVE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(NUTRIENT1_VALVE_PORT, NUTRIENT1_VALVE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(NUTRIENT2_VALVE_PORT, NUTRIENT2_VALVE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(NUTRIENT3_VALVE_PORT, NUTRIENT3_VALVE_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(MAIN_PUMP_PORT, MAIN_PUMP_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(AIR_PUMP_PORT, AIR_PUMP_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(PERISTALTIC_PUMP_PORT, PERISTALTIC_PUMP_PIN, gpioModePushPull, 1);

	deployValve(0);
	nutrientValve(0,1);
	nutrientValve(0,2);
	nutrientValve(0,3);

	GPIO_PinModeSet(WATER_LOW_LEVEL_PORT, WATER_LOW_LEVEL_PIN, gpioModeInputPullFilter, 1);
	GPIO_IntConfig(WATER_LOW_LEVEL_PORT, WATER_LOW_LEVEL_PIN, true, true, true);
	GPIOINT_CallbackRegister( WATER_LOW_LEVEL_PIN, WATER_LOW_LEVEL_PIN_irq );

	GPIO_PinModeSet(WATER_HIGH_LEVEL_PORT, WATER_HIGH_LEVEL_PIN, gpioModeInputPullFilter, 1);
	GPIO_IntConfig(WATER_HIGH_LEVEL_PORT, WATER_HIGH_LEVEL_PIN, true, true, true);
	GPIOINT_CallbackRegister( WATER_HIGH_LEVEL_PIN, WATER_HIGH_LEVEL_PIN_irq );

}

void deployValve(uint8_t state)
{
	if(!state)
	{
		GPIO_PinOutSet(DEPLOY_VALVE_PORT, DEPLOY_VALVE_PIN);
	}
	else
	{
		GPIO_PinOutClear(DEPLOY_VALVE_PORT, DEPLOY_VALVE_PIN);
	}
}

void nutrientValve(uint8_t state, uint8_t id)
{
	switch (id) {
	case NUTRIENT1:

		if(!state)
		{
			GPIO_PinOutSet(NUTRIENT1_VALVE_PORT, NUTRIENT1_VALVE_PIN);
		}
		else
		{
			GPIO_PinOutClear(NUTRIENT1_VALVE_PORT, NUTRIENT1_VALVE_PIN);
		}
		break;
	case NUTRIENT2:
		if(!state)
		{
			GPIO_PinOutSet(NUTRIENT2_VALVE_PORT, NUTRIENT2_VALVE_PIN);
		}
		else
		{
			GPIO_PinOutClear(NUTRIENT2_VALVE_PORT, NUTRIENT2_VALVE_PIN);
		}
		break;
	case NUTRIENT3:
		if(!state)
		{
			GPIO_PinOutSet(NUTRIENT3_VALVE_PORT, NUTRIENT3_VALVE_PIN);
		}
		else
		{
			GPIO_PinOutClear(NUTRIENT3_VALVE_PORT, NUTRIENT3_VALVE_PIN);
		}
		break;
	case WATER_NUTRIENT:
		if(!state)
		{
			GPIO_PinOutSet(NUTRIENT3_VALVE_PORT, NUTRIENT3_VALVE_PIN);
		}
		else
		{
			GPIO_PinOutClear(NUTRIENT3_VALVE_PORT, NUTRIENT3_VALVE_PIN);
		}
		break;
	default:
		break;
	}
}

uint8_t getHighLevelSensor()
{
	return high_level_sensor;
}

uint8_t getLowLevelSensor()
{
	return low_level_sensor;
}

const uint8_t *getHighLevelSensorPtr()
{
	return &high_level_sensor;
}

const uint8_t *getLowLevelSensorPtr()
{
	return &low_level_sensor;
}
