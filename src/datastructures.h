/*
 * datastructures.h
 *
 *  Created on: 25 de Out de 2015
 *      Author: Jos�
 */

#ifndef DATASTRUCTURES_H_
#define DATASTRUCTURES_H_

#include "stdint.h"

typedef enum
{
	Idle_nutrient_state = 0,
	Fill_water_before,
	Fill_nutrient,
	Deploy_nutrient,
	Fill_water_after,
	Deploy_water
}Nutrient_State;

typedef enum
{
	PumpOn = 0,
	PumpOff,

}Pump_State;

typedef enum
{
	 ControlAuto = 0,
	 ControlRaspi
}ControlMode_type;

typedef enum
{
	ControlMessage = 'C',
	HidroponicMessage = 'H',
	PhotovoltaicMessage = 'P'
}Radio_Messages;

typedef struct
{
	uint16_t ph;
	uint16_t temp[8];
	uint32_t rh;
	uint16_t ec;
	uint8_t unused[16];
}HydroponicNode_type;

typedef struct
{
	uint16_t node_id;
	uint8_t pwm1[2];
	uint8_t pwm2[2];
	uint8_t pwm3[2];
	uint8_t pwm4[2];

	uint16_t streaming_time;
	uint32_t timestamp;
	
	uint8_t day_light_hours[4];
	uint8_t day_light_pwm[4][4];
	uint8_t force_pwm[4];

}Control_Node;

typedef struct
{
	uint32_t pv_v;
	int32_t pv_i;
	uint32_t pv_p;
	uint32_t bat_v;
	int32_t bat_i;
	uint32_t bat_p;
	uint8_t unused[7];
}Photovoltaic_type;


#endif /* DATASTRUCTURES_H_ */
