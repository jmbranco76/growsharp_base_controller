/*
 * hall_reading.c
 *
 *  Created on: 25 de Ago de 2015
 *      Author: Jos�
 */

#include "hall_reading.h"

uint8_t value_sensor = 0;

volatile uint32_t sensors[NUM_SAMPLES], sensors_middle[NUM_SAMPLES];

void setCalib(I2C_TypeDef *i2c)
{
	memset(sensors_middle,0,sizeof(sensors_middle));
	// first calibration
	int i_calib = 0;
	int i_s = 0;
	for (i_calib = 0; i_calib < 100; ++i_calib) {

		readSensors();

		for (i_s = 0; i_s < NUM_SAMPLES; ++i_s) {

			sensors_middle[i_s] += sensors[i_s];
		}
	}
	for (i_s = 0; i_s < NUM_SAMPLES; ++i_s) {

		sensors_middle[i_s] /= 100;
	}

	writeCalibration(i2c, (uint8_t *)sensors_middle);
}

void getCalib(I2C_TypeDef *i2c)
{
	memset(sensors_middle,0,4*8);
	readCalibration(i2c, (uint8_t *)sensors_middle);
}
uint32_t * getSensors()
{
	return sensors;
}

HallSensor_States updateSensor(int limit, int8_t *cont_result)
{
	static uint8_t sensor_state = 0;
	static int8_t last_sensor = 0;
//	uint8_t sensor_above_limit[NUM_SAMPLES];
	static uint8_t n_times_sensor[NUM_SAMPLES];
	static uint8_t n_times_sensor_sum[NUM_SAMPLES];

	static HallSensor_States state = Init_State;
	MeasureState measure_state = Measuring;

	int i;

#ifdef ENABLE_DEBUG
	printf("state madefacker %d\n" , state);
#endif

	switch (state) {
	case Init_State:
		state = Default;

	case Default:
		memset(n_times_sensor, 0, NUM_SAMPLES);
		memset(n_times_sensor_sum, 0, NUM_SAMPLES);
		i = getSensorUpperLimit(limit, n_times_sensor);
		if(i != -1)
		{
#ifdef ENABLE_DEBUG
			printf("state 1 = %d\n", i);
#endif
			state = Check_For_False_Values;
			updateNlinmitSensors(n_times_sensor, n_times_sensor_sum, 5, 1);
			measure_state = Measuring;
		}
		break;

	case Check_For_False_Values:

		i = getSensorUpperLimit(limit, n_times_sensor);
		if(i != -1)
		{
#ifdef ENABLE_DEBUG
			printf("state 2 = %d\n", i);
#endif

			measure_state = updateNlinmitSensors(n_times_sensor, n_times_sensor_sum, 3, 0);
#ifdef ENABLE_DEBUG
			printf("measure_state 2 = %d\n", measure_state);
#endif

			switch (measure_state) {
			case MeasureOk:
				state = Check_For_Continuous;
				break;
			case ErrorMeasuring:
				state = Default;
				break;
			default:
				break;
			}
			break;
		}
		else
		{
			state = Default;
		}
		break;

	case Check_For_Continuous:
#ifdef ENABLE_DEBUG
		printf("state 3\n");
#endif
		*cont_result = checkForContinuity(last_sensor, n_times_sensor_sum);
		value_sensor = *cont_result;
		if(*cont_result != -1)
		{
			state = Default;
			last_sensor = *cont_result;
		}
		else
		{
			state = Error_State;
		}
		break;

		//TODO: acabar este processo de erro
	case Error_State:
		state = Default;
		break;
	default:
		break;
	}

	return state;
}

int8_t checkForContinuity(uint8_t last_sensor, uint8_t *n_times_sensor_sum)
{
	int i = 0;
	for (i = 0; i < NUM_SAMPLES; ++i)
	{
		if(n_times_sensor_sum[i])
		{
			break;
		}
	}
//	if(abs(i-(int)last_sensor) <= 1)
//	{
//		return i;
//	}

	return i;//-1;
}

MeasureState updateNlinmitSensors(uint8_t * vector, uint8_t * vector_sum, uint8_t n_valid_measures, uint8_t reset_state)
{
	int i = 0, sum_ok = 0;
	for (i = 0; i < NUM_SAMPLES; ++i)
	{
#ifdef ENABLE_DEBUG
		printf("%d,%d--", vector[i], vector_sum[i]);
#endif
	}
#ifdef ENABLE_DEBUG
	printf("\n");
#endif
	static MeasureState valid_measures = Measuring;
	if(reset_state)
	{
		valid_measures = Measuring;
	}

	for (i = 0; i < NUM_SAMPLES; ++i) {
		if(reset_state && vector[i])
		{
			vector_sum[i]= vector_sum[i]+1;
		}
		if(vector_sum[i])
		{
			sum_ok++;
		}
	}
	if(sum_ok)
	{
#ifdef ENABLE_DEBUG
		printf("sumok\n");
#endif
		for (i = 0; i < NUM_SAMPLES; ++i) {
			if(vector_sum[i] > 0)	// if value detected
			{
				if(vector[i] == 0)	// and in the next measure wasn't detected... is noise, so discard it
				{
					vector_sum[i]=0;
					valid_measures = ErrorMeasuring;
				}
				else if(vector[i]>0)
				{
					vector[i] = 0;
					vector_sum[i]= vector_sum[i]+1;
				}
				if(vector_sum[i] > n_valid_measures)
				{
					valid_measures = MeasureOk;
				}
			}


		}
#ifdef ENABLE_DEBUG
		printf("valid %d \n", valid_measures);
#endif
		return valid_measures;
	}
	if(reset_state)
	{
		return Measuring;
	}
	return ErrorMeasuring;
}

int8_t getSensorUpperLimit(int limit, uint8_t *results)
{
	int i = 0, f=0;
	uint8_t valid = 0;
	memset(results,0,NUM_SAMPLES);
	for (i = 0; i < NUM_SAMPLES; ++i)
	{
		if((int32_t)sensors[i]- (int32_t)sensors_middle[i] > limit)
		{
			results[i] = 1;
			valid = 1;
			if(f==0)
			{
				f = i;
			}
		}
		else
		{
			results[i] = 0;
		}
#ifdef ENABLE_DEBUG
		printf("%d-", results[i]);
#endif
	}
#ifdef ENABLE_DEBUG
	printf("\n");
#endif
	return valid ? f : -1;
}

int getLevel()
{
	return (value_sensor+0)*2;
}
